
const Tensao = require('../database/config').Tensao;
const Corrente = require('../database/config').Corrente;
const config = require('../config');
var mqtt = require('mqtt');

var topic = ['corrente', 'tensao'];
var client = mqtt.connect(config.broker_url + config.broker_port);
const active_topics = {
    async corrente(msg) {
        //console.log('corrente teste');
        await Corrente.create({ value: msg });
    },
    async tensao(msg) {
        //console.log('tensao teste');
        await Tensao.create({ value: msg });
    }
}
const sub = () => {
    client.on('connect', function () {
        client.subscribe(topic, function () {
            client.on('message', async function (topico, msg, pkt) {
                //console.log(topico);
                const excut_topic = active_topics[topico];
                if (excut_topic) {
                    excut_topic(msg.toString())
                }
            });
        });
    });

}

module.exports = sub;