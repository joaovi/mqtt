module.exports = (sequelize, DataTypes) => {
	var Corrente = sequelize.define('corrente', {
		value: { type: DataTypes.STRING(25), allowNull: false }
	});
	return Corrente;
};