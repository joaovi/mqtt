const Sequelize = require('sequelize');
const config = require('../config');

var database = {};
const sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: config.dialect,
    logging: false,
    define: {
        timestamps: false
    }
});

database.Corrente = require('./corrente-db')(sequelize, Sequelize.DataTypes);
database.Tensao = require('./tensao-db')(sequelize, Sequelize.DataTypes);
database.Sequelize = Sequelize;
database.sequelize = sequelize;

module.exports = database; 