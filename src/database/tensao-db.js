module.exports = (sequelize, DataTypes) => {
	var Tensao = sequelize.define('tensao', {
		value: { type: DataTypes.STRING(25), allowNull: false }
	});
	return Tensao;
};