module.exports = {
    broker_url: process.env.CLOUDMQTT_URL || 'mqtt://localhost:',
    broker_port: '1883',
    database: process.env.DB_NAME || 'mqtt-database',
    username: process.env.DB_USERNAME || 'postgres',
    password: process.env.DB_PASSWORD || '123456',
    host: process.env.DB_HOST || 'localhost',
    dialect: process.env.DB_DIALECT || 'postgres',
}
