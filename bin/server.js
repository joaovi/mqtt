const broker_ = require('../src/broker/broker');
const http = require('http');

const debug = require('debug')('nodestr:server');
const database = require('../src/database/config');
database.sequelize.authenticate()
    .then(async () => {
        broker_();
       
        await database.sequelize.sync();
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });



